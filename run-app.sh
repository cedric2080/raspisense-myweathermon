#!/bin/bash
# Parameter 1 gives the Environment. Given the value, we start with gunicorn of flask
#echo "UPGRADE to force flask to flush and start new db connection in case something went wrong"
#env/bin/python3 -m flask db upgrade

envchosen=$1

echo "Starting app in mode: $envchosen"
#if [[ $1 = "aaa" ]];
#if [[ $1 = "ProductionConfig" ]];
#if [[ $envchosen = "TestingConfig" ]] || [[ $envchosen = "ProductionConfig" ]];
#then
#  echo "Serving with Gunicorn ..."
#  env/bin/gunicorn -w 4 "com4care:create_app()" -b 0.0.0.0:8262 --log-level=info --error-logfile /var/log/com4care/com4care-backend.log
#else
#  echo "Serving with Flask dev server ..."
#  env/bin/python3 com4care.py
#fi
python3 main.py
