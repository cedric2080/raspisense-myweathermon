import threading
from time import sleep, time, process_time
from datetime import datetime
from curses import wrapper, endwin
import traceback

from sense_hat import SenseHat


class RepeatedTimer(object):
  #def __init__(self, interval, function, *args, **kwargs):
    def __init__(
            self,
            interval,
            n_measurements,
            offset_temperature,
            offset_temperature_from_humidity,
            offset_temperature_from_pressure,
            *args,
            **kwargs
            ):
        print("We initialise a Timer ...")
        # Timer data
        self._timer = None
        self.interval = interval # s
        # Measurements
        ## General config
        self.n_mesurements = n_measurements # number of measurements in 1 second
        self.average_period = 1
        self.interval_measurements = self.average_period / n_measurements # s
        self.average_duration_measurement_loop = 0.0 # ms
        self.offset_temperature = offset_temperature
        self.offset_temperature_from_humidity = offset_temperature_from_humidity
        self.offset_temperature_from_pressure = offset_temperature_from_pressure
        self.outfile = "monitor_data.txt"
        ## Temperature data
        self.measurements_temp = [0.0] * self.n_mesurements # degC
        self.measurements_temp_from_humidity = [0.0] * self.n_mesurements # degC
        self.measurements_temp_from_pressure = [0.0] * self.n_mesurements # degC
        self.measurements_pressure = [0.0] * self.n_mesurements # mBar
        self.measurements_humidity = [0.0] * self.n_mesurements # %relh
        self.average_temp = 0.0 # degC
        self.average_temp_from_humidity = 0.0 # %relh
        self.average_temp_from_pressure = 0.0 # mBar
        ## Pressure data
        self.measurements_pressure = [0.0] * self.n_mesurements # mBar
        self.average_pressure = 0.0 # mBar
        ## Humidity data
        self.measurements_humidity = [0.0] * self.n_mesurements # %relh
        self.average_humidity = 0.0 # %relh
        
        with open(self.outfile, "a") as file:
            file.write("Interval in between measurements: " + str(self.interval) + "s\n")
            file.write("Averaged number of measurements during " + str(self.average_period) + "s: " + str(self.n_mesurements) + "\n")
            file.write("New monitoring session\n")
            file.write("Date Time Start" +
                        " | Measurement loop average duration (ms)" +
                        " | Average Temperature (deg c) " +
                        " | Average Pressure (mBar)" +
                        " | Average Humidity (%rh)" +
                        " | Date Time End" +
                        "\n"
            )

        #self.function = function()
        self.args = args
        self.kwargs = kwargs
        self.is_running = False
        self.next_call = time()
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        #self.function(*self.args, **self.kwargs)
        self.monitor_temp(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self.next_call += self.interval
            self._timer = threading.Timer(self.next_call - time(), self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False

    def monitor_temp(self): 
        formated_string_start = datetime.now().__str__()
        average_temp = 0.0
        average_temp_from_humidity = 0.0
        average_temp_from_pressure = 0.0
        average_pressure = 0.0
        average_humidity = 0.0
        average_duration_measurement_loop = 0.0
        for index in range(self.n_mesurements):
            start = process_time()
            # Get raw data
            self.measurements_temp[index] = sense.get_temperature()
            self.measurements_temp_from_humidity[index] = sense.get_temperature_from_humidity()
            self.measurements_temp_from_pressure[index] = sense.get_temperature_from_pressure()
            self.measurements_pressure[index] = sense.get_pressure()
            self.measurements_humidity[index] = sense.get_humidity()
            #formated_string = "{:,.1f}".format(self.measurements[index])
            #print("Temperature: %s C" % formated_string)
            # Avergae data
            average_temp += self.measurements_temp[index]
            average_temp_from_humidity += self.measurements_temp_from_humidity[index]
            average_temp_from_pressure += self.measurements_temp_from_pressure[index]
            average_pressure += self.measurements_pressure[index]
            average_humidity += self.measurements_humidity[index]
            average_duration_measurement_loop += process_time()
            average_duration_measurement_loop -= start
            sleep(self.interval_measurements)
        self.average_temp = average_temp / self.n_mesurements + self.offset_temperature
        self.average_temp_from_humidity = average_temp_from_humidity / self.n_mesurements + self.offset_temperature_from_humidity
        self.average_temp_from_pressure = average_temp_from_pressure / self.n_mesurements + self.offset_temperature_from_pressure
        self.average_pressure = average_pressure / self.n_mesurements
        self.average_humidity = average_humidity / self.n_mesurements
        self.average_duration_measurement_loop = 1000 * average_duration_measurement_loop / self.n_mesurements
        
        formated_string_end = datetime.now().__str__()
        formated_string_0 = "{:.3f}".format(self.average_duration_measurement_loop)
        formated_string_1 = "{:,.1f}".format(self.average_temp)
        formated_string_4 = "{:,.1f}".format(self.average_pressure)
        formated_string_5 = "{:,.1f}".format(self.average_humidity)
        with open(self.outfile, 'a') as file:
            file.writelines(
                [
                    (
                        formated_string_start +
                        " | %s" % formated_string_0 +
                        " | %s" % formated_string_1 +
                        " | %s" % formated_string_4 +
                        " | %s" % formated_string_5 +
                        " | " + formated_string_end +
                        "\n"
                    ),
                ]
            )


def main(stdscr):
    global offset_temperature, offset_temperature_from_humidity, offset_temperature_from_pressure
    #rt = RepeatedTimer(10, monitor_temp) # it auto-starts, no need of rt.start()
    n_measurements = 25 # number of measurements to be averaged along 1 second
    fdisplay = 10*1.5 # Interval in s between refreshing printouts, default was 10 for default sense scroll
    interval = 60 # Interval in s between measurements
    # We have a task repeated at some interval, involving only sensing and dumping values to a txt file
    rt = RepeatedTimer(
        interval,
        n_measurements,
        offset_temperature,
        offset_temperature_from_humidity,
        offset_temperature_from_pressure
    ) # it auto-starts, no need of rt.start()
    # In parallel, we have the printing code that refresh at some display frequency with the value held
    #  by the last monitoring timed task
    try:
        while True:
            # Clear screen
            stdscr.clear()
            for index in range(n_measurements):
                formated_string_1 = "{:,.1f}".format(rt.measurements_temp[index])
                formated_string_2 = "{:,.1f}".format(rt.measurements_temp_from_humidity[index])
                formated_string_3 = "{:,.1f}".format(rt.measurements_temp_from_pressure[index])
                stdscr.addstr(
                    index, 0,
                    (
                        "Temperature: %s C" % formated_string_1 +
                        " | Temperature from humidity: %s C" % formated_string_2 +
                        " | Temperature from pressure: %s C" % formated_string_3
                    )
                )
            formated_string = datetime.now().__str__()
            formated_string_0 = "{:.3f}".format(rt.average_duration_measurement_loop)
            formated_string_1 = "{:,.1f}".format(rt.average_temp)
            formated_string_2 = "{:,.1f}".format(rt.average_temp_from_humidity)
            formated_string_3 = "{:,.1f}".format(rt.average_temp_from_pressure)
            formated_string_4 = "{:,.1f}".format(rt.average_pressure)
            formated_string_5 = "{:,.1f}".format(rt.average_humidity)
            stdscr.addstr(
                index+1, 0,
                (
                    "Average Temperature: %s C" % formated_string_1 +
                    " | Average Temperature from humidity: %s C" % formated_string_2 +
                    " | Average Temperature from pressure: %s C" % formated_string_3
                )
            )
            stdscr.addstr(
                index+2, 0,
                (
                    "Average Pressure: %s mBar" % formated_string_4 +
                    " | Average relative humidity: %s %%rh" % formated_string_5
                )
            )
            stdscr.addstr(index+3, 0, ("Measurement loop average duration: %s ms" % formated_string_0))
            stdscr.addstr(index+4, 0, ("%s" % formated_string))
            stdscr.refresh()
            #led_string = formated_string_1 + chr(176) + "c - " + formated_string_5 + "%"
            led_string = formated_string_1 + " - " + formated_string_5 + "%"
            #sense.show_message(formated_string_1)
            #sense.show_message(formated_string_5+"%")
            if rt.average_temp >= 24:
                colormessage = colors['red']
            elif rt.average_temp >= 23 and rt.average_temp < 24:
                colormessage = colors['orange']
            elif rt.average_temp >= 22.5 and rt.average_temp < 23:
                colormessage = colors['yellorange']
            elif rt.average_temp >= 22 and rt.average_temp < 22.5:
                colormessage = colors['yellow']
            elif rt.average_temp >= 21 and rt.average_temp < 22:
                colormessage = colors['greenyellow']
            elif rt.average_temp >= 19 and rt.average_temp < 21:
                colormessage = colors['green']
            else:
                colormessage = colors['blue']

            sense.show_message(
                led_string,
                scroll_speed = scroll_speed,
                text_colour = colormessage
            )
            sleep(fdisplay)
    except:
        endwin()
        traceback.print_exc()
    finally:
        rt.stop() # better in a try/finally block to make sure the program ends!
        endwin()
    print("End")

global offset_temperature, offset_temperature_from_humidity, offset_temperature_from_pressure
global colors
global scroll_speed

## Setup
colors = {}
colors['black'] = (1,1,1)
colors['white'] = (255,255,255)
#red = (207,8,31)
colors['red'] = (252,36,3)
colors['orange'] = (252,161,3)
colors['yellorange'] = (252,198,3)
colors['yellow'] = (248,252,3)
colors['greenyellow'] = (148,252,3)
colors['green'] = (148,252,3)
colors['blue'] = (3,157,252)


# Scale temperature sensor
temperature_sense_hat = 22.9
temperature_reference = 20.1
offset_temperature = temperature_reference - temperature_sense_hat

# set SenseHat
sense = SenseHat()
sense.set_rotation(180)
sense.low_light = True
scroll_speed = 0.25 # default is 0.1

## Probe Environment
print("Init monitoring...")
humidity = sense.get_humidity()
formated_string = "{:,.1f}".format(humidity)
print("Humidity: %s %%rH" % formated_string)

pressure = sense.get_pressure()
formated_string = "{:,.0f}".format(pressure)
print("Pressure: %s Millibars" % formated_string)

temp = sense.get_temperature()
formated_string = "{:,.1f}".format(temp)
print("Temperature: %s C" % formated_string)
formated_string = "{:,.1f}".format(temp+offset_temperature)
print("Temperature scaled: %s C" % formated_string)

# With the following measurements, a new offset can be found
temp_from_humidity = sense.get_temperature_from_humidity()
formated_string = "{:,.1f}".format(temp_from_humidity)
print("Temperature from humidity: %s C" % formated_string)
offset_temperature_from_humidity = temp - temp_from_humidity + offset_temperature
formated_string = "{:,.1f}".format(temp_from_humidity+offset_temperature_from_humidity)
print("Temperature from humidity scaled: %s C" % formated_string)


temp_from_pressure = sense.get_temperature_from_pressure()
formated_string = "{:,.1f}".format(temp_from_pressure)
print("Temperature from pressure: %s C" % formated_string)
offset_temperature_from_pressure = temp - temp_from_pressure + offset_temperature
formated_string = "{:,.1f}".format(temp_from_pressure+offset_temperature_from_pressure)
print("Temperature from pressure scaled: %s C" % formated_string)

sense.clear()

print("Start monitoring scheduler")
print("Press Enter to start ...")
input()

wrapper(main)
